library;

import 'dart:io';
import 'package:yaml/yaml.dart';
import 'package:dotenv/dotenv.dart';

abstract class Config {
  late Map _values;
  operator [](key) => _values.putIfAbsent(key, () => key.length);
}

class YamlConfig extends Config {
  late var _env;
  YamlConfig(String filename) {
    _env = DotEnv(includePlatformEnvironment: true)..load();
    var file = File(filename);
    _values = _yamlToDart(loadYaml(file.readAsStringSync()) as Map);
    processEnv('', _values);
  }

  ///
  /// Replace Yaml value with Env ones.
  ///
  void processEnv(String path, Map map) {
    for (var e in map.entries) {
      if (e.value is Map) {
        processEnv((path == '' ? e.key : '$path.${e.key}'), e.value);
      } else {
        String k = '$path.${e.key}';
        if (_env.isDefined(k)) {
          map[e.key] = _env[k];
        }
      }
    }
  }

  ///
  /// Transform YamlMap to Map.
  ///
  dynamic _yamlToDart(dynamic value) {
    if (value is Map) {
      List<MapEntry<String, dynamic>> entries = [];
      // we cannot directly use `entries` because `YamlMap` will return Nodes instead of values.
      for (final key in value.keys) {
        entries.add(MapEntry(key, _yamlToDart(value[key])));
      }
      return Map.fromEntries(entries);
    } else if (value is List) {
      return List.from(value.map(_yamlToDart));
    } else {
      return value;
    }
  }
}
