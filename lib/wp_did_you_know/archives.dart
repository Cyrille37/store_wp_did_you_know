///
/// Retrouve les archives des anecdotes
///
/// https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Le_saviez-vous_%3F/Archives/2023
///

library;

import 'package:http/http.dart' as http;
import 'package:html/dom.dart';
import 'package:html/dom_parsing.dart';
import 'package:html/parser.dart';
import 'package:injector/injector.dart';
import 'package:wp_did_you_know/wp_did_you_know/data.dart';

Future wpDidYouKnowArchives(int year) async {
  Data? data = Injector.appInstance.get<Data?>();

  final uri = Uri.https(
      'fr.wikipedia.org', '/wiki/Wikipédia:Le saviez-vous ?/Archives/$year');
  print(uri.toString());
  final response = await http.get(uri);
  if (response.statusCode != 200) {
    throw Exception('Failed to get page ${response.statusCode}');
  }

  var doc = parse(response.body).documentElement!;
  // div#mw-content-text.mw-body-content div.mw-content-ltr.mw-parser-output ul
  var elements = doc.querySelectorAll(
      'div[id="mw-content-text"].mw-body-content div.mw-content-ltr.mw-parser-output ul li');

  print('Wikipedia "Le Saviez-vous" ? Achives $year');
  print('elements count: ${elements.length}\n');

  /*
  var visitor = _Visitor();
  for (Element element in elements) {
    print(element.runtimeType.toString());
    String item = await visitor.extract(element);
    print(item);
    if (data != null) data.store(item);
  }
  */
  List<Future<void>> tasks = <Future<void>>[];
  for (Element element in elements) {
    tasks.add(_Visitor()
        .extract(element)
        .then((String item) => handleItem(data, item)));
  }
  return Future.wait(tasks);
}

handleItem(Data? data, String item) {
  data != null ? data.store(item) : print('$item\n');
}

// Note: this example visitor doesn't handle things like printing attributes and
// such.
class _Visitor extends TreeVisitor {
  String result = '';

  Future<String> extract(Node node) async {
    result = "";
    super.visit(node);
    return result;
  }

  @override
  void visitText(Text node) {
    if (node.data.trim().isNotEmpty) {
      //print('$indent${node.data.trim()}');
    }
    result += node.data;
  }

  @override
  void visitElement(Element node) {
    if (node.localName == 'figure') return;
    if (node.localName == 'dl') {
      return;
    }

    if (isVoidElement(node.localName)) {
    } else {
      switch (node.localName) {
        case 'a':
          var href = node.attributes["href"];
          if (href!.startsWith('/wiki/')) {
            result += '<a href="$href">';
          }
          break;
        case 'li':
          break;
        default:
          result += '<${node.localName!}>';
      }
      visitChildren(node);
      switch (node.localName) {
        case 'li':
          break;
        default:
          result += '</${node.localName!}>';
      }
    }
  }

  @override
  void visitChildren(Node node) {
    for (var child in node.nodes) {
      visit(child);
    }
  }
}
